
import java.util.ArrayList;
import java.util.Scanner;

public class PersonDB {

    static Scanner kb = new Scanner(System.in);

    static String username;
    static String password;
    static int cmd;

    public static void play() {
        while (true) {
            if (login()) {
                while (true) {
                    showMenu();
                    cmd = selectMenu();
                    if (cmd == 1) {
                        addData();
                    } else if (cmd == 2) {
                        showData();
                    } else if (cmd == 3) {
                        editData();
                    } else if (cmd == 4) {
                        if (logout()) {
                            break;
                        }
                    }
                }
            } else {
                break;
            }
        }
    }

    public static boolean login() {
        while (true) {
            showLogin();
            inputLogin();
            if (username.equals("exit")) {
                return false;
            }
            if (data.checkLogin(username, password)) {
                return true;
            } else {
                showErrorLogin();
                continue;
            }
        }
    }

    public static void showLogin() {
        System.out.println("Login");
    }

    public static void showErrorLogin() {
        System.out.println("Username or password not correct!!");
    }

    public static void inputLogin() {
        kb = new Scanner(System.in);
        System.out.print("Username : ");
        username = kb.next();
        if (username.equals("exit")) {
            return;
        }
        System.out.print("Password : ");
        password = kb.next();
    }

    public static void showMenu() {
        System.out.println("----Menu----");
        System.out.println("1. เพิ่มข้อมูล\n"
                            + "2. ดูข้อมูล\n"
                            + "3. แก้ไขข้อมูล\n"
                            + "4. ออกจากระบบ");
    }

    public static int selectMenu() {
        while (true) {
            try {
                int cmd = kb.nextInt();
                if (cmd > 0 && cmd < 5) {
                    return cmd;
                } else {
                    showErrorSelectMenu();
                }
            } catch (Exception e) {
                kb.next();
                showErrorSelectMenu();
            }
        }
    }

    public static void showErrorSelectMenu() {
        System.out.println("Plase choose number 1 - 4");
    }

    public static void addData() {
        String name = addName();
        String Surname = addSurname();
        int Age = addAge();
        String Username = addUsername();
        String Password = addPassword();
        data.persons.add(new Person(name, Surname, Age, Username, Password));
        data.saveData();
    }

    public static String addName() {
        while (true) {
            System.out.print("Name :");
            String name = kb.next();
            if (!(name.equals(""))) {
                return name;
            } else {
                showErrorName();
            }
        }
    }

    public static void showErrorName() {
        System.out.println("Name can't empty");
    }

    public static String addSurname() {
        while (true) {
            System.out.print("Surname :");
            String Surname = kb.next();
            if (!(Surname.equals(""))) {
                return Surname;
            } else {
                showErrorSurname();
            }
        }
    }

    public static void showErrorSurname() {
        System.out.println("Surname can't empty");
    }

    public static int addAge() {
        while (true) {
            System.out.print("Age :");
            String age = kb.next();
            try {
                return Integer.parseInt(age);
            } catch (Exception e) {
                showErrorAge();
                continue;
            }
        }
    }

    public static void showErrorAge() {
        System.out.println("Age must number");
    }

    public static String addUsername() {
        while (true) {
            System.out.print("Username :");
            String Username = kb.next();
            if (!(data.checkUsernameisHave(Username))) {
                return Username;
            } else {
                showErrorUsername();
            }
        }
    }

    public static void showErrorUsername() {
        System.out.println("Username have already");
    }

    public static String addPassword() {
        while (true) {
            System.out.print("Password :");
            String Password = kb.next();
            if (Password.length() >= 8) {
                return Password;
            } else {
                showErrorPassword();
            }
        }
    }

    public static void showErrorPassword() {
        System.out.println("Password must be 8 characters long");
    }

    public static void showData() {
        data.showData();
        while (true) {
            showSelectMenuData();
            try {
                cmd = kb.nextInt();
                if (cmd == 1) {
                    chooseUsername();
                } else {
                    break;
                }
            } catch (Exception e) {
                kb.next();
                break;
            }
        }
    }

    public static void showSelectMenuData() {
        System.out.println("---ShowData---");
        System.out.println("1. Search from Username");
        System.out.println("2. Back to menu");
    }

    public static void chooseUsername() {
        System.out.print("Search Username : ");
        String Username = kb.next();
        if (data.checkUsernameisHave(Username)) {
            showDetailUser(Username);
        } else {
            showUserNotFound(Username);
        }
    }

    static void showDetailUser(String username) {
        for (Person person : data.persons) {
            if (person.getUsername().equals(username)) {
                System.out.println("-----------");
                System.out.println("---" + username + "---");
                System.out.println("Name : " + person.getName());
                System.out.println("Surname : " + person.getSurname());
                System.out.println("Age : " + person.getAge());
                System.out.println("-----------");
            }
        }
    }

    public static void showUserNotFound(String username) {
        System.err.println("User " + username + " not found");
    }

    public static void editData() {
        if (username.equals("admin")) {
            System.out.println("Admin can't edit");
        } else {
            while (true) {
                showEditMenu();
                cmd = selectEditMenu();
                if (cmd == 1) {
                    editName();
                } else if (cmd == 2) {
                    editSurname();
                } else if (cmd == 3) {
                    editAge();
                } else if (cmd == 4) {
                    editPassword();
                } else if (cmd == 5) {
                    break;
                }
                data.saveData();
            }
        }
    }

    public static void showEditMenu() {
        System.out.println("1. แก้ไขชื่อ\n"
                + "2. แก้ไขนามสกุล\n"
                + "3. แก้ไขอายุ\n"
                + "4. แก้ไขรหัสผ่าน\n"
                + "5. ย้อนกลับไปหน้าเมนู");
    }

    public static int selectEditMenu() {
        while (true) {
            try {
                int cmd = kb.nextInt();
                if (cmd > 0 && cmd < 6) {
                    return cmd;
                } else {
                    showErrorSelectEditMenu();
                }
            } catch (Exception e) {
                kb.next();
                showErrorSelectEditMenu();
            }
        }
    }

    public static void showErrorSelectEditMenu() {
        System.out.println("Plase choose number 1 - 5");
    }

    public static void editName() {
        while (true) {
            System.out.print("Name :");
            String Name = kb.next();
            if (!(Name.equals(""))) {
                for (int i = 0; i < data.persons.size(); i++) {
                    if (data.persons.get(i).getUsername().equals(username)) {
                        data.persons.get(i).setName(Name);
                        break;
                    }
                }
                break;
            } else {
                showErrorName();
            }
        }
    }

    public static void editSurname() {
        while (true) {
            System.out.print("Surname :");
            String Surname = kb.next();
            if (!(Surname.equals(""))) {
                for (int i = 0; i < data.persons.size(); i++) {
                    if (data.persons.get(i).getUsername().equals(username)) {
                        data.persons.get(i).setSurname(Surname);
                        break;
                    }
                }
                break;
            } else {
                showErrorSurname();
            }
        }
    }

    public static void editAge() {
        while (true) {
            System.out.print("Age :");
            String Age = kb.next();
            try {
                for (int i = 0; i < data.persons.size(); i++) {
                    if (data.persons.get(i).getUsername().equals(username)) {
                        data.persons.get(i).setAge(Integer.parseInt(Age));
                        break;
                    }
                }
                break;
            } catch (Exception e) {
                showErrorAge();
                continue;
            }
        }
    }

    public static void editPassword() {
        while (true) {
            System.out.print("Password :");
            String Password = kb.next();
            if (Password.length() >= 8) {
                for (int i = 0; i < data.persons.size(); i++) {
                    if (data.persons.get(i).getUsername().equals(username)) {
                        data.persons.get(i).setPassword(Password);
                        break;
                    }
                }
                break;
            } else {
                showErrorPassword();
            }
        }
    }

    public static boolean logout() {
        showLogout();
        if (confirmLogout()) {
            return true;
        } else {
            return false;
        }
    }

    public static void showLogout() {
        System.out.println("Confirm Logout");
        System.out.println("Input 1 to Logout");
        System.out.println("Input 0 to Cancel");
    }

    public static boolean confirmLogout() {
        while (true) {
            try {
                cmd = kb.nextInt();
                if (cmd == 1 || cmd == 0) {
                    if (cmd == 1) {
                        return true;
                    } else if (cmd == 0) {
                        return false;
                    }
                } else {
                    showLogoutError();
                }
            } catch (Exception e) {
                showLogoutError();
                kb.next();
            }

        }
    }

    public static void showLogoutError() {
        System.out.println("Plase input number 0 or 1");
    }

}
