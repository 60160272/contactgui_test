
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author hafish
 */
public class data {

    public static ArrayList<Person> persons = new ArrayList<>();

    static {
        getDataFromFile("persons.bin");
    }
    static void load(){
        getDataFromFile("persons.bin");
    }
    static boolean checkLogin(String username, String password) {
        if (username.equals("admin") && password.equals("admin")) {
            return true;
        }
        for (Person person : persons) {
            if (person.getUsername().equals(username) && (person.getPassword().equals(password))) {
                return true;
            }
        }
        return false;
    }

    static void showData() {
//        for (Person person : persons) {
//            System.out.println(person.toString());
//        }
        readUserFormFile("persons.bin");
    }

    static void saveData() {
        if (writeUserToFile("persons.bin")) {
            System.out.println("Success");
        } else {
            System.out.println("Error");
        }
    }

    static boolean checkUsernameisHave(String username) {

        return false;
    }

    public static boolean writeUserToFile(String fileName) {
        boolean chk = true;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            fos = new FileOutputStream(fileName);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(persons);

        } catch (FileNotFoundException ex) {
            chk = false;
        } catch (IOException ex) {
            chk = false;
        }

        if (oos != null) {
            try {
                oos.close();
            } catch (IOException ex) {
                chk = false;
            }
        }
        if (fos != null) {
            try {
                fos.close();
            } catch (IOException ex) {
                chk = false;
            }
        }
        return chk;

    }

    public static void readUserFormFile(String fileName) {
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        persons = null;

        try {
            fis = new FileInputStream(fileName);
            ois = new ObjectInputStream(fis);
            persons = (ArrayList<Person>) ois.readObject();
            for (Person person : persons) {
                System.out.println("Username : " + person.getUsername());
                System.out.println("Name : " + person.getName());
                System.out.println("Surname : " + person.getSurname());
                System.out.println("Age : " + person.getAge());
                System.out.println("-----------");
            }
        } catch (FileNotFoundException ex) {
            System.err.println("Error File naja");
        } catch (IOException ex) {
            System.err.println("Error IO naja");
        } catch (ClassNotFoundException ex) {
            System.err.println("Error Class naja");
        }

        if (persons != null) {
            try {
                fis.close();
            } catch (IOException ex) {
                System.err.println("Error");
            }
        }
    }

    public static void getDataFromFile(String fileName) {
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        persons = null;

        try {
            fis = new FileInputStream(fileName);
            ois = new ObjectInputStream(fis);
            persons = (ArrayList<Person>) ois.readObject();
        } catch (FileNotFoundException ex) {
            System.err.println("Error File naja");
        } catch (IOException ex) {
            System.err.println("Error IO naja");
        } catch (ClassNotFoundException ex) {
            System.err.println("Error Class naja");
        }

        if (persons != null) {
            try {
                fis.close();
            } catch (IOException ex) {
                System.err.println("Error");
            }
        }
    }
}
