
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author hafish
 */
public class PersonTableModel extends AbstractTableModel {

    static ArrayList<Person> arr = data.persons;
    String colunmNames[] = {"NAME","SURNAME", "AGE", "USERNAME", "PASSWORD"};

    @Override
    public String getColumnName(int column) {
        return colunmNames[column];
    }

    @Override
    public int getRowCount() {
        return arr.size();
    }

    @Override
    public int getColumnCount() {
        return colunmNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Person person = arr.get(rowIndex);
        if (person == null) {
            return "";
        }
        switch (columnIndex) {
            case 0:
                return person.getName();
            case 1:
                return person.getSurname();
            case 2:
                return person.getAge();
            case 3:
                return person.getUsername();
            case 4:
                return person.getPassword();
        }
        return "";
    }

}
